#=== README Starts =======

# Overview

This project contains the working code for the Automation Technical Challenge tests:
1. UI automation test scenario using Selenium:
- Navigate to https://www.commbank.com.au/
- Go to Bank Account > Everyday Account Smart Access > Open Now > Open Now > Get Started > Fill
the details - Now tell us about yourself > OK

Note: Since this test stops midway during the registration process the shutdown process which closes the windows and quit the webdriver is turned off so the last screen can be seen.

- The test class for this is au.org.standards.tests.CBATest which can be run as a JUnit test.
- The page object model classes can be found under au.org.standards.pageobjects directory.

2. Rest API automation test scenario:

- Register to get the API token key - https://www.weatherbit.io/account/create
- Navigate to https://www.weatherbit.io/api/swaggerui/weather-api-v2#!/and automate below APIs
  a. GET /current?lat={lat}&lon={lon}for values {lat} as 40.730610 and {lon} as -73.935242
  It should parse the response and get the value of the /data/state_code
  b. GET /forecast/3hourly?postal_code={postal_code}
  
The test class for this is au.org.standards.tests.WeatherBitTest which can be run as a JUnit test
It uses au.org.standards.client.RestClient to handle Rest requests and responses.

# Build and run instructions
1. Make sure Maven plugin is installed in your IDE

2. Import the project into your IDE, convert it into a Maven project and build. 
If the project fails to build due to dependencies then take a look at the pom.xml file 
which contains all dependency configurations for this project.

3. Run the tests in CBATest and WeatherBitTest as JUnit tests.
The UI automation test (CBATest) requires a webdriver to run so remember on change the setting to the webdriver path on your environment. This setting is in the CBATest.setUp() method.

#====== END OF FILE ===========================

