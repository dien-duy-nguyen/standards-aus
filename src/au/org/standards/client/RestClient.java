package au.org.standards.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class RestClient 
{
	// The base URL for the web service
	private final String m_baseUrl;
	
	// The client used for executing the actual requests
	private HttpClient m_client;
	
	// The parameters for the request
	private Map<String, String> m_parameters;
	
	/**
	 * COnstructor
	 * @param p_baseUrl The base URL to the web service
	 */
	public RestClient(String p_baseUrl) 
	{
		m_baseUrl = p_baseUrl;
		m_client = new DefaultHttpClient();
		m_parameters = new HashMap<String, String>();
	}
	
	/**
	 * Add a parameter to the request's URL
	 * @param p_name The name of the parameter
	 * @param p_value The value for the parameter
	 */
	public void addParam(String p_name, String p_value) 
	{
		m_parameters.put(p_name, p_value);
	}
	
	/**
	 * Convenient method to add multiple parameters in name, value pairs.
	 * Note: The input must be a parameter name followed by its value in that order.
	 * 
	 * @param p_name Name of first parameter
	 * @param p_value Value for the first parameter
	 * @param p_additionalParams Additional parameters in name, value pairs.
	 * @throws IllegalArgumentException If the number of additional parameters is not even.
	 */
	public void addParams(String p_name, String p_value, String... p_additionalParams) 
	{
		addParam(p_name, p_value);
		
		// add the additional params
		if (p_additionalParams != null) 
		{
			// make sure the additional params come in name, value pairs
			int additionalLength = p_additionalParams.length;
			if (additionalLength % 2 == 1) 
			{
				throw new IllegalArgumentException("Additional parameters must come in name value pairs, ie, must be an even number.");
			}
			for (int i=0; i<additionalLength - 1; i = i + 2) 
			{
				addParam(p_additionalParams[i], p_additionalParams[i+1]);
			}
		}
	}
	
	/**
	 * Remove the given parameter from the request's URL
	 * @param p_name Name of the parameter to be removed
	 */
	public void removeParam(String p_name) 
	{
		m_parameters.remove(p_name);
	}
	
	/**
	 * Get the URL string for the request
	 * @return The URL as a string
	 */
	public String getRequestURL() 
	{
		StringBuilder builder = new StringBuilder(m_baseUrl);
		boolean paramAdded = false;
		if (!m_parameters.isEmpty()) {
			for (Entry<String, String> entry : m_parameters.entrySet())
			{
				// if a parameter has been added then append & for this param, else append ?
				if (paramAdded) 
				{
					builder.append("&");
				}
				else
				{
					builder.append("?");
					paramAdded = true;
				}
				
				builder.append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		return builder.toString();
	}

	/**
	 * Execute the GET request
	 * @return The corresponding response
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse executeGet() throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet(getRequestURL());
		return m_client.execute(request);
	}
}
