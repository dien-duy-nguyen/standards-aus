package au.org.standards.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParser.Event;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.junit.Test;

import au.org.standards.client.RestClient;


/**
 * Test the Rest end-points for current and forecast/3hourly for the site
 * - https://www.weatherbit.io/api/swaggerui/weather-api-v2
 * 
 * @author dien.nguyen
 */
public class WeatherBitTest 
{
	// The API key for the rest end-points
	public static final String API_KEY = "7822357e48cf4272b81a6af311580693";
	
	/**
	 *  GET /current?lat={lat}&lon={lon}for values {lat} as 40.730610 and {lon} as -73.935242
	 *  The expected state code is NY 
	 * @throws Exception
	 */
	@Test
	public void testGetCurrentByLocation() throws Exception
	{
		String latitude = "40.730610";
		String longitude = "-73.935242";
		
		// construct the request and execute it
		String baseUrl = "https://api.weatherbit.io/v2.0/current";
		RestClient restClient = new RestClient(baseUrl);
		restClient.addParams("lat", latitude, "lon", longitude, "key", API_KEY);
		HttpResponse response = restClient.executeGet();
		
		// make sure the status code is OK
		int statusCode = response.getStatusLine().getStatusCode();
		assertTrue("Status code is not 200", statusCode == HttpStatus.SC_OK);
		
		// parse the response for the state code
		// use Streaming API parser for efficiency
		InputStream is = response.getEntity().getContent();
		String stateCode = "";
		JsonParser parser = Json.createParser(is);
		while (parser.hasNext()) 
		{
			Event event = parser.next();
			if (event == Event.KEY_NAME) 
			{
				String key = parser.getString();
				if (key.equals("state_code"))
				{
					parser.next();
					stateCode = parser.getString();
					System.out.print("state code = " + stateCode);
					break;
				}
			}
		}
		assertEquals("State code does not match", "NY", stateCode);
	}
	
	/**
	 * GET /forecast/3hourly?postal_code={postal_code} 
	 * Since the postal_code is not specified in the test specification I'll use the one from the website's example
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetForecastByPostalcode() throws Exception
	{
		String postal_code = "28546"; // from website example
		
		// construct and execute the request
		String baseUrl = "https://api.weatherbit.io/v2.0/forecast/3hourly";
		RestClient client = new RestClient(baseUrl);
		client.addParam("postal_code", postal_code);
		HttpResponse response = client.executeGet();
		
		// verify that the status code is 403
		int statusCode = response.getStatusLine().getStatusCode();
		System.out.println("Status code for forecast = " + statusCode);
		assertTrue("Status code is not 403", statusCode == HttpStatus.SC_FORBIDDEN);
	}
}
