package au.org.standards.tests;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import au.org.standards.pageobjects.Title;
import au.org.standards.pageobjects.cba.ApplicationFormPage;
import au.org.standards.pageobjects.cba.BankAccountsPage;
import au.org.standards.pageobjects.cba.CBAHomePage;
import au.org.standards.pageobjects.cba.EverydayAccountPage;
import au.org.standards.pageobjects.cba.ExistingOrNewCustomerPopupPage;
import au.org.standards.pageobjects.cba.ReadyToApplyPage;

/**
 * Test the CBA web site
 * @author dien.nguyen
 *
 */
public class CBATest 
{

	private static WebDriver driver;

	@BeforeClass
	public static void setUp()
	{
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver", "C:/webdrivers/chromedriver.exe");
		driver = new ChromeDriver();

		//Applied wait time
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		//maximise window
		driver.manage().window().maximize();
	}

	@After
	public void cleanUp()
	{
		driver.manage().deleteAllCookies();
	}

	@AfterClass
	public static void tearDown() 
	{
		// commented out users can see the last screen of the test
//		driver.close();
//		driver.quit();
	}
	
	@Test
	public void testSmartAccessNewCustomer() throws Exception
	{
		CBAHomePage homePage = new CBAHomePage(driver);
		homePage.open();
		BankAccountsPage bankAccountsPage = homePage.navigateToBankAccounts();
		EverydayAccountPage everydayAccountPage = bankAccountsPage.navigateToEverydayAccount();
		ExistingOrNewCustomerPopupPage popupPage = everydayAccountPage.openNow();

		// Store the current window handle
//		String mainWindowHandle = driver.getWindowHandle();

		// Perform the actions on new window
		ReadyToApplyPage readyToApplyPage = popupPage.openNow();
		
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		// Perform the click operation that opens new window
		ApplicationFormPage formPage = readyToApplyPage.getStarted();
		formPage.fillDetails(Title.MR, "Dien", "Duy", "Nguyen");
		
		// ensure that the EmailAndMobile component is visible
		assertTrue("Email and mobile component is not displayed as expected", formPage.getContactInfoComponent().isDisplayed());
		
		// Close the new window, if that window no more required
//		driver.close();

		// Switch back to original browser (first window)
//		driver.switchTo().window(mainWindowHandle);

		// Continue with original browser (first window)
	}
}
