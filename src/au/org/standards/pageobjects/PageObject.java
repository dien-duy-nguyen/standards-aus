package au.org.standards.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * The parent class for all page objects.
 * This class contains common methods for children page object classes to use.
 * @author dien.nguyen
 *
 */
public abstract class PageObject {
	
	// The webdriver to use
	private WebDriver driver;
	
	/**
	 * Create a PageObject with the given webdriver
	 * @param p_driver The web driver
	 */
	protected PageObject(WebDriver p_driver) {
		this.driver = p_driver;
	}
	
	/**
	 * Get the webdriver associated with this page object
	 * @return The page object's web driver
	 */
	public WebDriver getDriver() {
		return driver;
	}
	
	protected void clickUsingActions(WebElement p_webElement) 
	{
		Actions actions = new Actions(driver);
		actions.moveToElement(p_webElement).click().perform();
	}
	
	/**
	 * Perform click on the button with an implicit wait of 10 seconds and maximum 3 tries.
	 * @param p_elementLocator Locator used to find the element
	 */
	protected void clickButton(By p_elementLocator) 
	{
		clickWithWaitAndRetries(p_elementLocator, 10, 3);
	}
	
	/**
	 * Wait the specified number of seconds until the element with the given locator method is clickable then click it
	 * If the click fails try again until max attempts is reached.
	 * @param p_elementLocator
	 * @param p_seconds
	 * @param p_maxAttempts The number of maximum tries
	 */
	protected void clickWithWaitAndRetries(By p_elementLocator, int p_seconds, int p_maxAttempts) 
	{
		WebElement element = null;
		int attempts = 0;
		while (attempts < p_maxAttempts) {
			WebDriverWait wait = new WebDriverWait(getDriver(), p_seconds);
			element = wait.until(ExpectedConditions.elementToBeClickable(p_elementLocator));
			try
			{
				// if click is successful we're done
				element.click();
				break;
			}
			catch (Exception ex) 
			{
				// else, try again
				++attempts;
			}
		}
	}
	
	/**
	 * Wait for 60 seconds until the element is visible
	 * @param element
	 * @throws Error
	 */
	protected void waitForVisibility(WebElement element)
	{
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * Wait for the text field to be visible in the DOM then fill it with the given text
	 * @param p_element
	 * @param p_text
	 */
	protected void fillTextElement(WebElement p_element, String p_text) 
	{
		waitForVisibility(p_element);
		p_element.sendKeys(p_text);
	}
}
