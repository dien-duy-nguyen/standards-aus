package au.org.standards.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Encapsulate the new user details on the ApplicationFormPage
 * @author dien.nguyen
 *
 */
public class UserDetailsComponent extends PageObject {

	@FindBy(id = "Title")
	private WebElement titleDropBox;
	
	@FindBy(id = "FirstName")
	private WebElement firstNameField;
	
	@FindBy(id = "MiddleNames")
	private WebElement middleNamesField;
	
	@FindBy(id = "LastName")
	private WebElement lastNameField;
	
	@FindBy(id="nameSubmitButton")
	private WebElement nameSubmitButton;
	
	public UserDetailsComponent(WebDriver p_driver) {
		super(p_driver);
	}
	
	/**
	 * Submit the details for this component by filling the details and click its OK button
	 * @param p_title Title of the user
	 * @param p_firstName First name of the person
	 * @param p_middleNames Middle name of the person, could be null
	 * @param p_lastName Last name of the person
	 */
	public ContactInfoComponent submitDetails(Title p_title, String p_firstName, String p_middleNames, String p_lastName) 
	{
		// select the title
		selectTitleFromDropdownList(p_title.ordinal());
		
		// fill in the name fields
		fillTextElement(firstNameField, p_firstName);
		if (p_middleNames != null && !p_middleNames.isEmpty()) 
		{
			// middle name is optional
			fillTextElement(middleNamesField, p_middleNames);
		}
		fillTextElement(lastNameField, p_lastName);
		
		// click the OK button
		nameSubmitButton.click();
		
		ContactInfoComponent emailMobileComponent = PageFactory.initElements(getDriver(), ContactInfoComponent.class);
		return emailMobileComponent;
	}
	
	/**
	 * Select the title corresponding to the given index from the drop-down lost
	 * @param p_index The index of the title in the drop-down list, with first being 0.
	 */
	private void selectTitleFromDropdownList(int p_index) 
	{
	      Select dropbox = new Select(titleDropBox);
	      dropbox.selectByIndex(p_index + 1);  
	}
}
