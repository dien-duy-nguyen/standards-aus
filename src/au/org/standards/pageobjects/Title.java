/**
 * 
 */
package au.org.standards.pageobjects;

/**
 * Enumeration list of a person's title as shown on the new customer page form
 * @author dien.nguyen
 *
 */
public enum Title 
{
	MR, MS, MRS, MISS
}
