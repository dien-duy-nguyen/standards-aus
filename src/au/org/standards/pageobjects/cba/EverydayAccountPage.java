package au.org.standards.pageobjects.cba;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.PageObject;

/**
 * Represent the Everyday account page
 * @author dien.nguyen
 *
 */
public class EverydayAccountPage extends PageObject 
{
	
	@FindBy(linkText = "Open now")
	private WebElement openNowLink;

	public EverydayAccountPage(WebDriver p_driver) 
	{
		super(p_driver);
	}
	
	/**
	 * Click on the Open now link
	 * @return
	 */
	public ExistingOrNewCustomerPopupPage openNow()
	{
		clickUsingActions(openNowLink);
		return PageFactory.initElements(getDriver(), ExistingOrNewCustomerPopupPage.class);
	}
}
