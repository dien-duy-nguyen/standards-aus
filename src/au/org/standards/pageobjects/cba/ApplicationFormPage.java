package au.org.standards.pageobjects.cba;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.ContactInfoComponent;
import au.org.standards.pageobjects.PageObject;
import au.org.standards.pageobjects.Title;
import au.org.standards.pageobjects.UserDetailsComponent;

public class ApplicationFormPage extends PageObject {

	@FindBy(id="landingSubmitButton")
	private WebElement landingSubmitButton;
	
	@FindBy(id = "singleSubmitButton")
	private WebElement singleSubmitButton;
	
	@FindBy(id="btnHasConcessionCardNo")
	private WebElement noConcessionCardButton;
	
	private UserDetailsComponent userDetails;
	
	private ContactInfoComponent contactInfo;
	
	public ApplicationFormPage(WebDriver p_driver) {
		super(p_driver);
	}
	
	/**
	 * Fill the user's details
	 * @param p_title The user's title
	 * @param p_firstName The user's first name
	 * @param p_middleNames The user's middle names, which is optional
	 * @param p_lastName The user's last name
	 */
	public void fillDetails(Title p_title, String p_firstName, String p_middleNames, String p_lastName) 
	{
		landingSubmitButton.click();
		
		clickButton(By.id("singleSubmitButton"));
		clickButton(By.id("btnHasConcessionCardNo"));

		userDetails = PageFactory.initElements(getDriver(), UserDetailsComponent.class);
		contactInfo = userDetails.submitDetails(p_title, p_firstName, p_middleNames, p_lastName);
	}
	
	public UserDetailsComponent getUserDetailsComponent()
	{
		return userDetails;
	}
	
	public ContactInfoComponent getContactInfoComponent()
	{
		return contactInfo;
	}
}
