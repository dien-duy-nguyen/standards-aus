package au.org.standards.pageobjects.cba;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.PageObject;

/**
 * Represent the Ready to apply page
 * @author dien.nguyen
 */
public class ReadyToApplyPage extends PageObject {
	
	@FindBy(linkText = "Get started")
	private WebElement getStartedLink;
	
	public ReadyToApplyPage(WebDriver p_driver) {
		super(p_driver);
	}
	
	/**
	 * Click the Get started link
	 * @return
	 */
	public ApplicationFormPage getStarted()
	{
		getStartedLink.click();
		return PageFactory.initElements(getDriver(), ApplicationFormPage.class);
	}
}
