package au.org.standards.pageobjects.cba;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.PageObject;

/**
 * Represents the CBA home page
 * @author dien.nguyen
 *
 */
public class CBAHomePage extends PageObject 
{
	
	//Page URL
	private final String PAGE_URL="https://www.commbank.com.au/";

	@FindBy (linkText ="Bank accounts")
	private WebElement bankAccountsButton;

	public CBAHomePage(WebDriver p_driver) 
	{
		super(p_driver);
		PageFactory.initElements(getDriver(), this);
	}
	
	/**
	 * Navigate to the Bank accounts page
	 * @return
	 */
	public BankAccountsPage navigateToBankAccounts() 
	{
		// click on the link
		clickUsingActions(bankAccountsButton);
		
		return PageFactory.initElements(getDriver(), BankAccountsPage.class);
	}
	
	public void open() 
	{
		getDriver().get(PAGE_URL);
	}
	
	public String getUrl() 
	{
		return PAGE_URL;
	}
}
