package au.org.standards.pageobjects.cba;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.PageObject;

/**
 * Represent the Bank account page
 * @author dien.nguyen
 *
 */
public class BankAccountsPage extends PageObject 
{
	@FindBy(xpath = "//a[@href=\"/banking/everyday-accounts.html\"]")
	private WebElement everydayAccountLink;
	
	public BankAccountsPage(WebDriver p_driver) 
	{
		super(p_driver);
	}
	
	/**
	 * Navigate to the Everyday account page
	 * @return The EverydayAccountPage object
	 */
	public EverydayAccountPage navigateToEverydayAccount()
	{
		// click on the link
		clickUsingActions(everydayAccountLink);
		return PageFactory.initElements(getDriver(), EverydayAccountPage.class);
	}
}
