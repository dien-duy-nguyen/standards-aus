package au.org.standards.pageobjects.cba;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import au.org.standards.pageobjects.PageObject;

/**
 * Represent the Existing or new customer pop up page
 * @author dien.nguyen
 *
 */
public class ExistingOrNewCustomerPopupPage extends PageObject {

	@FindBy(linkText = "Open in NetBank")
	private WebElement openInNetBankLink;
	
	@FindBy(xpath = "//a[@href=\"/banking/ready-to-apply.html\"]")
	private WebElement openNowLink;
	
	public ExistingOrNewCustomerPopupPage(WebDriver p_driver) {
		super(p_driver);
	}
	
	/**
	 * Click the Open now link
	 * @return
	 */
	public ReadyToApplyPage openNow()
	{
		openNowLink.click();
		return PageFactory.initElements(getDriver(), ReadyToApplyPage.class);
	}
}
