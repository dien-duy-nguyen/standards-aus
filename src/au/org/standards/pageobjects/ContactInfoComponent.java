package au.org.standards.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Encapsulating the contact information of a user.
 * @author dien.nguyen
 *
 */
public class ContactInfoComponent extends PageObject {

	@FindBy(id = "email")
	private WebElement emailField;
	
	@FindBy(id = "mobile")
	private WebElement mobileField;
	
	@FindBy(id = "emailMobileSubmitButton")
	private WebElement okButton;
	
	/**
	 * @param p_driver
	 */
	public ContactInfoComponent(WebDriver p_driver) {
		super(p_driver);
	}
	
	/**
	 * Submit the email and mobile number and click the OK button
	 * @param p_email
	 * @param p_mobileNumber
	 */
	public void submitDetails(String p_email, String p_mobileNumber) 
	{
		emailField.sendKeys(p_email);
		mobileField.sendKeys(p_mobileNumber);
		okButton.click();
	}
	
	/**
	 * Whether the component is displayed on the page
	 * @return True if the component is displayed, false otherwise
	 */
	public boolean isDisplayed()
	{
		return emailField.isDisplayed() && mobileField.isDisplayed() && okButton.isDisplayed();
	}
}
